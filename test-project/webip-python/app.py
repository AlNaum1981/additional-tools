import os
import socket
from flask import Flask

hostname = socket.gethostname()
ip_address = socket.gethostbyname(hostname)

app = Flask(__name__)

@app.route("/")
def main():
    return f"Hostname: {hostname} IP Address: {ip_address}"


if __name__ == "__main__":
    app.run()