1) Установка ingress-nginx

1.1 добавляем репу
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

1.2 сохраняем файл конфига
helm show values ingress-nginx/ingress-nginx > values.yaml

1.3 заходим vi values.yaml и меняем параметры hostNetwork, hostPort на true, kind на DaemonSet и применяем.
пример в статье https://habr.com/ru/articles/713520/

1.4 создаем ns
kubectl create ns ingress-nginx

1.5 устанавливаем
helm install ingress ingress-nginx/ingress-nginx -n ingress-nginx --values values.yaml

2) Установка metallb

2.1 скачал deploy по ссылке 
https://metallb.universe.tf/installation/

2.2 установливаем, обязательно проверять свежесть версии
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.12/config/manifests/metallb-native.yaml

2.3 создаем ConfigMap со своими ip
https://gitlab.com/k8s-highload-project/additional-tools/-/blob/main/ingress-metallb/mlb.yaml
